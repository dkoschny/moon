"""File: generate_moon_ephemerides.py

Calls the routine `get_moon_geometry.py` and writes a file in the format as
needed by the routine `track_moon.py`.

Usage
-----
python generate_moon_ephemerides.py <start_time> <end_time> <obs_loc>

Method
------
Read start and end time and location (IAU code only for now). It will query
the JPL Horizons system to get the Moon ephemerides in 1-minute intervals.
Note: No error checking is performed in this routine, we rely on the checking
by JPL Horizons.

Version
-------
V1.0, 22 Jan 2022, dvk - First complete version.

"""

import numpy as np
from get_moon_geometry import get_moon_geometry
from datetime import *
import csv
import sys

# ----------------------------------------------------------------------------
# Define constants/variables.
# ----------------------------------------------------------------------------
# start and end time could be passed via the command line.
print(sys.argv)

if len(sys.argv) == 4:
    print("Found command line arguments.")
    start_time = sys.argv[1]
    end_time = sys.argv[2]
    obs_loc = sys.argv[3]

else:
    start_time = "2022-01-21T20:00:00"
    end_time = "2022-01-21T20:10:00"

d_t = "1%20m"
obs_loc = "B12"
file_name = "moon_ephemerides.csv"

f = open(file_name, "w", newline="")    # Opening the csv file.
csvwriter = csv.writer(f)               # Define a writer object.

# ----------------------------------------------------------------------------
# Get the data.
# ----------------------------------------------------------------------------
time, RA, Dec, obs_sub_lat, obs_sub_lon, sun_sub_lat, \
    sun_sub_lon, dist = get_moon_geometry(start_time, end_time, d_t, obs_loc)

if len(time) == 0:
    print("")
    print("ERROR - JPL Horizons didn't return anything. Possible reasons: " + \
          "(a) No connection to the internet; (b) start time after end " + \
          "time; (c) obs code not known; (d) format of start or end time " + \
          "not correct or out of range.")

# Write to a csv file. Note that we get the data in decimal degrees, but
# ASCOM wants decimal hours. Thus the division by 15 (15 deg per hour).
for i in np.arange(0, len(time)):
    csvwriter.writerow([time[i], float(RA[i]) / 15, float(Dec[i])])
    
f.close()
print("Ending.")
# ================================== eof =====================================
