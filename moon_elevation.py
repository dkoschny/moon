"""File: moon_elevation.py

Plotting Moon altitude in astropy.

moon_phase_angle and moon_illumination inspired by:
https://docs.astropy.org/en/stable/generated/examples/
coordinates/plot_obs-planning.html

Usage
-----
>>python moon_elevation.py B12 2022-02-04

This will produce a plot of the Moon conditions as seen from B12
(the Koschny Observatory) for the night 2022-02-04 to 2022-02-05.

Versions
--------
V0.6, 30 Jan 2022, dvk - adding reading of IAU obs codes, r_earth
                       - add lower left plot - simple az/el for now.
V0.7, 04 Feb 2022, dvk - added horizon.
V0.8, 20 May 2022, dvk - editorials.
V0.9, 01 Nov 2022, dvk - improved performance when computing
                         rise and set times by using `astroplan`.

Still open
----------
- Add the computation of Moon appearance, disappearance behind real horizon.
- Force the y-axis label of the upper right plot to always have one decimal.
- Add Moon culmination time.
- Should I sort the time printing in the upper left panel by time?
- Add time stamps to lower left plot.
- Add little images showing the Moon phase.
- Add colors for daytime, twilight, darkness.
- Make sun_rise and sun_set work on Southern hemisphere.

"""

# -----------------------------------------------------------------------------
# Import the required libraries.
# -----------------------------------------------------------------------------
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import matplotlib.ticker as mticker
from astropy.coordinates import SkyCoord, EarthLocation, AltAz
from astropy.coordinates import get_moon, get_sun
import astropy.units as u
from astropy.time import Time
from astropy.visualization import astropy_mpl_style, quantity_support
from astropy.visualization import time_support
from astroquery.mpc import MPC
from astroplan import Observer
import csv
import sys


# ----------------------------------------------------------------------------
# Define required routines.
# ----------------------------------------------------------------------------
def r_earth(lati):
    """Computing the radius of the Earth at a given latitude.
    
    Parameters
    ----------
    geo_lati : float
        Geodetic latitude in radians.
        
    Returns
    -------
    radius : float
        Radius of the Earth in m.
        
    """
    r_eq = 6378137.0
    r_po = 6356752.0
    
    radius = np.sqrt(((r_eq**2 * np.cos(lati))**2 +  \
                      (r_po**2 * np.sin(lati))**2) / \
                     ((r_eq * np.cos(lati))**2 + \
                      (r_po * np.sin(lati))**2))
    return radius
    
    
# ----------------------------------------------------------------------------
def get_geo_coordinates(obs_code):
    """
    Determine the longitude, latitude, and height of your site from
    an IAU observatory code.
    
    Parameters
    ----------
    obs_code : string
        A three-letter string (can have numbers) to describe your IAU
        observatory code. E.g. 'B12' is the Koschny Observatory, 'J04'
        ESA's Optical Ground Station on Tenerife.
        
    Returns
    -------
    geo_long : float
        Longitude of the location, in decimal degrees
        
    geo_lati : float
        Latitute of the location, in decimal degrees
        
    geo_height : float
        Height above geoid in m.

    """
    r_eq = 6378137.0  # Earth radius at the equator.

    # Find the entry for the given code. If not found, return.
    try:
        obs = MPC.get_observatory_location(obs_code)
    except:
        raise Exception("No internet connection, or obsrvatory code "+ \
                        f"{obs_code} not found.")
    
    geo_long = obs[0].value  # Needs `value` as this in a `quantity`
    geo_lati = np.arctan(obs[2] / obs[1]) * 180/np.pi
    geo_height = r_eq * obs[1] / np.cos(geo_lati * np.pi/180) \
               - r_earth(geo_lati * np.pi/180)

    return geo_long, geo_lati, geo_height


# ----------------------------------------------------------------------------
def read_horizon(horizon_file_name):
    """Reading the horizon.
    
    Parameters
    ----------
    horizon_file_name : string
        File name containing the data - expects a csv file with
        azimuth and elevation.
        
    Returns
    -------
    A tuple of azimuth/elevation pairs.
    
    """
    horizon_data = []
    with open(horizon_file_name, newline="") as csv_file:
        csv_reader = csv.reader(csv_file)
        try:
            for row in csv_reader:
                horizon_data.append([float(row[0]), float(row[1])])
        except:
            raise Exception("csv file not in the right format.")
            
    return horizon_data
    
    


# ----------------------------------------------------------------------------
def moon_phase_angle(time):
    """
    Calculate orbital phase of the Moon in radians.

    Parameters
    ----------
    time : astropy.time.Time array
        Times of observation.

    Returns
    -------
    astropy.units.Quantity
        Phase angle of the Moon in radians

    """

    sun = get_sun(time)
    moon = get_moon(time)
    elongation = sun.separation(moon)
    return np.arctan2(sun.distance * np.sin(elongation),
                      moon.distance - sun.distance * np.cos(elongation))


# ----------------------------------------------------------------------------
def moon_illumination(time):
    """
    Compute illuminated fraction of the Moon.
    
    Parameters
    ----------
    time : astropy.time.Time
        Time of observation

    Returns
    -------
    Illuminated fraction of the Moon ranging from 0 to 1

    """
    i = moon_phase_angle(time)
    return (1 + np.cos(i)) / 2.0


# -----------------------------------------------------------------------------
def moon_elevation(time, location):
    """Compute the Moon elevation in degrees

    Parameters
    ----------
    time : astropy.time.Time
        The time

    Returns
    -------
    Elevation of the Moon in degrees

    """
    moon_coords = get_moon(time, location=location)
    moon_altaz = moon_coords.transform_to(AltAz(location=location))
    return moon_altaz.alt


# -----------------------------------------------------------------------------
def moon_azimuth(time, location):
    """Compute the Moon azimuth in degrees

    Parameters
    ----------
    time : astropy.time.Time array
        time

    Returns
    -------
    Azimuth of the Moon in degrees

    """
    moon_coords = get_moon(time, location=location)
    moon_altaz = moon_coords.transform_to(AltAz(location=location))
    return moon_altaz.az
    

# -----------------------------------------------------------------------------
def plot_upper_left(time, loc):
    """Plotting upper left pane with text.
    
    Parameters
    ----------
    time : astropy.time.Time
        Array with times.
        
    loc : astropy.EarthLocation
        The location of the observatory.
        
    Details
    -------
    In detail, the following text is shown:
    - location with lat, long, height
    - Moon rise and set times
    - Times of twilight end and begin
    - Moon appearance and disappearanc time, taking the
      real horizon into account.
 
    """
    line = np.arange(1, 0, -0.08)
    # Switch off the frame and ticks
    axs[0][0].set(frame_on=False)
    axs[0][0].xaxis.set_major_locator(mticker.NullLocator())
    axs[0][0].yaxis.set_major_locator(mticker.NullLocator())

    # Write text
    axs[0][0].text(0, line[0], r"$\bf{Location}$")
    axs[0][0].text(0, line[1], f"longitude: {loc.lon.value:9.5f}" + \
                               " deg (East is positive)")
    axs[0][0].text(0, line[2], f"latitude: {loc.lat.value:9.5f} deg")
    axs[0][0].text(0, line[3], f"height: {loc.height.value:.1f} m - " + \
                               f"Note: error about 70 m!")
    axs[0][0].text(0, line[4], "")
    axs[0][0].text(0, line[5], r"$\bf{Events}$")
    axs[0][0].text(0, line[6], f"Moonrise: {moonrise}")
    axs[0][0].text(0, line[7], "Moon appearance: to come")
    axs[0][0].text(0, line[8], "Moon culminates: to come")
    axs[0][0].text(0, line[9], "Moon disappears: to come")
    axs[0][0].text(0, line[10], f"Moon sets: {moonset}")
    axs[0][0].text(0, line[11], f"Sun sets: {sunset}")
    axs[0][0].text(0, line[12], f"Sun rises: {sunrise}")


# -----------------------------------------------------------------------------
def plot_upper_right(time, loc):
    """Plotting the upper right pane: Delta Dec and Delta RA.
    
    Method
    ------
    We take the RA/Dec of the Moon and compute the differences.
    These we plot, shifted by 1/2 of the delta time.
    
    """
    # Determine RA and Dec of the Moon
    moon_coords = get_moon(time, location=loc)
    
    # Compute the differences
    delta_ra = np.diff(moon_coords.ra.value) * 3600 * u.arcsec / u.min
    delta_dec = np.diff(moon_coords.dec.value) * 3600 * u.arcsec / u.min
    
    # Shift the time axis by 1/2 the minimum step, call it d_time,
    # reduce length by one
    d_time = time + 0.5 * u.min
    d_time = d_time[:-1]
    axs[0][1].plot(d_time.value, delta_ra)
    axs[0][1].set_ylabel("Delta RA in " + str(delta_ra.unit))
    axs[0][1].set_ylim([25, 50])
    
    # Set up the right y axis for the declination change.
    right_axis = axs[0][1].twinx()
    right_axis.set_ylabel("Delta Dec in " + str(delta_dec.unit), color="green")
    right_axis.yaxis.set_major_formatter(mticker.FormatStrFormatter("%-3.1f"))
    right_axis.plot(d_time.value, delta_dec, color="green")
    
    # Join the x axis with the one on top - see here:
    # https://www.semicolonworld.com/question/55572/
    # how-share-x-axis-of-two-subplots-after-they-are-created
    interval = 86400 * 2 # This needs to be such that the labels in the plot
                         # are reasonably spaced
    axs[0][1].get_shared_x_axes().join(axs[0][1], axs[1][1]) 

    # Switch off tick labels as it will be linked to the lower plot.
    axs[0][1].set_xticklabels([])
    axs[0][1].xaxis.set_major_locator(mdates.MinuteLocator(interval=86400))
    axs[0][1].yaxis.set_major_formatter(mticker.FormatStrFormatter("%-3.1f"))
    axs[0][1].grid()
    

# -----------------------------------------------------------------------------
def plot_lower_left(time, loc):
    """Plotting the lower left pane: elev over azimuth.
    
    Parameters
    ----------
    time : astropy.time.Time
        Array of times
    
    Returns
    -------
    The plot... note that it will need `plt.show()` to
    display something.
    
    """
    elev = moon_elevation(time, loc)
    azim = moon_azimuth(time, loc)
    horizon_data = read_horizon("horizon.csv")

    axs[1][0].set_xlim([45, 315])
    axs[1][0].set_ylim([min_elev, max_elev])
    
    # First plot all points from start time to sunset in grey.
    azim1 = azim[np.where(time < sunset)]
    elev1 = elev[np.where(time < sunset)]
    axs[1][0].scatter(azim1, elev1, s=1, c="grey")
    
    # Plot points from sunset to sunrise # in blue.
    azim2 = azim[np.where(np.logical_and(time > sunset, time < sunrise))]
    elev2 = elev[np.where(np.logical_and(time > sunset, time < sunrise))]
    axs[1][0].scatter(azim2, elev2, s=2, c="blue", label="Sun below horizon")
    
    # Lastly, plot everything after sunrise in grey again.
    azim3 = azim[np.where(time > sunrise)]
    elev3 = elev[np.where(time > sunrise)]
    axs[1][0].scatter(azim3, elev3, s=1, c="grey", label="Sun above horizon")
    
    # Plot the horion as lines connecting the points.
    h = np.transpose(horizon_data)
    axs[1][0].plot(h[0], h[1], "-g")
    
    axs[1][0].set_xlabel("Azimuth in " + str(azim.unit))
    axs[1][0].set_ylabel("Elevation in " + str(elev.unit))
    axs[1][0].legend(bbox_to_anchor=(0.5, -0.3))
    axs[1][0].grid()


# -----------------------------------------------------------------------------
def plot_lower_right(time, loc):
    """Plotting the lower right pane: Elevation vs. time.
    
    Parameters
    ----------
    time : astropy.time.Time
        Array of times
    
    Returns
    -------
    The plot... note that it will need `plt.show()` to
    display something.
    
    """
    elev = moon_elevation(time, loc)
    axs[1][1].plot(time.value, elev)
    axs[1][1].set_ylim([min_elev, max_elev])
    axs[1][1].set_xlabel("Date/Time")
    axs[1][1].set_ylabel("Elevation in " + str(elev.unit))
    
    interval = 86400 * 2
    axs[1][1].xaxis.set_major_locator(mdates.MinuteLocator(interval=interval))
    plt.setp(axs[1][1].get_xticklabels(), rotation=60, ha="right")
    axs[1][1].grid()

# ======================== Main program starting here. ========================
# -----------------------------------------------------------------------------
# Define needed variables.
# -----------------------------------------------------------------------------
program_start_time = Time.now()
print("Starting `moon_elevation.py`")

if len(sys.argv) == 3:
    obs_code = sys.argv[1]
    start_date = sys.argv[2]
else:
    obs_code = "B12"
    start_date = "2022-02-15"

# Read observing location.
geo_long, geo_lati, geo_height = get_geo_coordinates(obs_code)
obs_loc = EarthLocation(lon=geo_long * u.deg, lat=geo_lati * u.deg, \
                        height=geo_height * u.m)

# Define an `astroplan` observer to compute rise and set times
ap_obs = Observer(location=obs_loc)

# Setting start and end time of plot.
start_time = Time(start_date + " 12:00:00", format="iso", out_subfmt="date_hm")
end_time = start_time + 1  # Always 1 day

fig_size = (16/1.5, 9/1.5)
min_elev = 0                    # Minimum shown elevation in all plots.
max_elev = 60                   # Maximum shown elevation in all plots.

horizon_file_name = "horizon.csv"

# Read horizon file.
with open(horizon_file_name, mode ="r") as file:
    # reading the CSV file
    horizon = csv.reader(file)  # Should contain azimuth and elevationho

# -----------------------------------------------------------------------------
# Compute delta times, time steps, sunrise, sunset.
# -----------------------------------------------------------------------------
delta_time = end_time - start_time
step_in_days = 1 / (delta_time.value)
step_in_hours = 1 / (delta_time.value * 24)
step_in_minutes = 1 / (delta_time.value * 24 * 60)

# Creating an array of `time` with given step size. It looks
# like for daily plots, a step size of minutes is best.
time = start_time + delta_time * np.arange(0, 1, step_in_minutes)

sunrise = ap_obs.sun_rise_time(time=end_time).iso
sunset = ap_obs.sun_set_time(time=start_time).iso
moonrise = ap_obs.moon_rise_time(time=start_time, which="next").iso
moonset = ap_obs.moon_set_time(time=start_time, which="next").iso

"""
sunset = "2022-02-15 18:00"
sunrise = "2022-02-16 07:00"
moonrise = "2022-02-15 19:00"
moonset = "2022-02-16 08:00"
"""

# -----------------------------------------------------------------------------
# Do the work.
# -----------------------------------------------------------------------------
# Enable proper time support in matplotlib.
time_support()

# Set up a 2 x 2 plot. The upper left area is used only for text.
fig, axs = plt.subplots(2, 2, figsize=fig_size, constrained_layout=True)
fig.set_facecolor("whitesmoke")
fig.suptitle('Moon viewing conditions ' + time[0].value.split(' ')[0],
             fontsize=14)

# Plot everything
print("Starting the plot generation", end="")
plot_upper_left(time, obs_loc)
plot_upper_right(time, obs_loc)
plot_lower_left(time, obs_loc)
plot_lower_right(time, obs_loc)

out_file_name = obs_code + "_" + start_date + ".png"
plt.savefig(out_file_name, format="png")
print()
print("File written: ", out_file_name)

program_end_time = Time.now()
print("Run time was ", program_end_time.value - program_start_time.value)
print(" - Done.")
