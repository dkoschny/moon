"""File: track_moon.py

Read a file with the coordinates of the Moon, send ASCOM commands to a mount
to track the Moon using this file.

Description
-----------
- Use `get_moon_geometry.py` to produce a csv file with time, RA, Dec

- Save the data in a file called `moon_ephemeris.csv`. A time step of
  one minute is suggested, but feel free to reduce this if you have a
  large field of view. This is done in this way so that you can use this
  routine without being connected to the internet (as in Detlef's garden).

- Then call this routine to do the following:
    . connect to your telescope
	. you then set your telescope to the position on the Moon you want
	  to have in the center - say 'synchronize'.
    . This routine will the read the file (default name: moon_ephemeris.csv)...
    . and send the necessary ASCOM commands to a mount to track the Moon.
    
NOTE: For my Celestron AVX mount this means that for every slew I loose a
      few seconds as it first moves the telescope away from the position,
      then returns slowly. This is the backlash compensation, which needs
      to be switched off to avoid this behavior.
  
Details
-------
This is what this routine does:

1. Connect to your telescope via ASCOM
2. Read the input file `moon_ephemeris.csv`
3. Set the current position to whereever you want it to be - it then
   assumes you are somewhere on the Moon and want to keep that position.
4. Start a loop to check the current time - assume that the starting position
   is the first position in the file.
5. At the next time step, take the difference in coordinates and move the
   telescope to track the Moon's movement.
6. When the input file has ended, give a message and quit.

Dependencies
------------
This tool requires the ephemerides of the Moon in a CSV file. These can
be generated with a file `generate_moon_ephemerides.py`, which in turn
requires `get_moon_geometry.py`.

Versions
--------
V1.0, 23 Jan 2022, dvk - First version, tested with Celestron AVX mount.

"""
# ----------------------------------------------------------------------------
# Define required functions.
# ----------------------------------------------------------------------------
def position_vs_time(schedule, time):
    """Interpolate a position in RA/Dec for the given time from the
    provided schedule file.
    
    Method
    ------
    1. Go through the schedule and find the time steps next to the `time`.
    2. Interpolate the precise position.
    3. Return this - if the time is outside the schedule, then exit.

    """
    # First check whether the reqested time is in the schedule at all.
    # If not, exit the program.
    if time < schedule[0][0] or time > schedule[-1][0]:
        logging.error(" - Time is not covered by the schedule. Ending.")
        print("Time is not covered by the schedule. Ending.")
        sys.exit()

    # Otherwise, continue.
    i = 0
    while time > schedule[i][0]:
        last_time = schedule[i][0]
        i += 1
    
    RA = (schedule[i][1] + schedule[i + 1][1]) / 2
    Dec = (schedule[i][2] + schedule[i + 1][2]) / 2
    
    return RA, Dec


# ----------------------------------------------------------------------------
# Import required libraries.
# ----------------------------------------------------------------------------
import win32com.client                # Needed to connect to ASCOM objects.
from datetime import datetime as dt   # Needed for time computations.
from datetime import timedelta
import logging
import csv
import sys
import time

# ----------------------------------------------------------------------------
# Define variables and constants.
# ----------------------------------------------------------------------------
file_name = "moon_ephemerides.csv"
try:
    f = open(file_name, "r")
except:
    logging.error(f" - File {file_name} not found.")
    sys.exit()

schedule = []
    
# ----------------------------------------------------------------------------
# Set up logging.
# ----------------------------------------------------------------------------
log_file_name = "track_moon.log"
# There seems to be an issue with writing something in the log file - see:
# https://stackoverflow.com/questions/15892946/
# python-logging-module-is-not-writing-anything-to-file
for handler in logging.root.handlers[:]:
    logging.root.removeHandler(handler)

logging.basicConfig(filename=log_file_name, encoding='utf-8',
        level=logging.DEBUG, format='%(asctime)s %(levelname)s %(message)s',
        datefmt='%Y-%m-%dT%H:%M:%S')
logging.info("")
logging.info(" - Started.")

# ----------------------------------------------------------------------------
# 1. Connect to your telescope.
# ----------------------------------------------------------------------------
try:
    t_name = win32com.client.Dispatch("ASCOM.Utilities.Chooser").choose(None)
    # t_name = "ScopeSim.Telescope"  # Just for testing, saves one click.
    tel = win32com.client.Dispatch(t_name)
    tel.Connected = True
except:
    logging.error(" - Cancelled connection, or connection error " + \
                  "generated by ASCOM. Terminated program.")
    sys.exit()

# Here we find out whether the telescope is really there.
try:
    tel.Unpark()
    tel.Tracking = True
    logging.info(f" - Mount was found: {t_name}.")
except:
    logging.error(" - Could not connect to telescope mount.")
    print("Could not connect to telescope mount. Terminated program.")
    sys.exit()

# ----------------------------------------------------------------------------
# 2. Read input file, replace first column (string) with datetime object
#    and the rest with float. The csv file contains as the first element
#    the time, the second is RA, the third is Dec.
# ----------------------------------------------------------------------------
with open(file_name, "r") as file:
    csv_file = csv.reader(file)
    for line in csv_file:
        schedule.append([dt.strptime(line[0], " %Y-%b-%d %H:%M:%S"), 
                         float(line[1]), float(line[2])])

# ----------------------------------------------------------------------------
# 3. Set current position - Assume that the telescope is at the current
#    position (given the current time) as given in the input file for 
#    the time the user presses return.
# ----------------------------------------------------------------------------
input("sMove telescope to your favorite position on the Moon. Then press " + \
      "return and I will start tracking this point on the Moon.")
now = dt.utcnow()
RA, Dec = position_vs_time(schedule, now)
tel.SyncToCoordinates(RA, Dec)
logging.info(f" - Synchronizing mount position to RA = {RA:07.4f} h " + \
             f"and Dec = {Dec:07.4f} deg.")


# ----------------------------------------------------------------------------
# 4. Start loop to check current time - assume starting position is the first
#    position in the file.
# ----------------------------------------------------------------------------
end_time = schedule[-1][0]
current_time = dt.utcnow()

# Remove all entries in the past.
while current_time > schedule[0][0]:
    schedule.pop(0)

# Finally, start the 'real-time' loop.
while current_time < end_time:
     # -----------------------------------------------------------------------
     # 5. Check every second whether a new pointing position should
     #    be commanded.
     # -----------------------------------------------------------------------
     if current_time > schedule[0][0]:
        # Time has reached first entry in schedule - slew telescope.
        RA = schedule[0][1]
        Dec = schedule[0][2]
        tel.SlewToCoordinates(RA, Dec)
        logging.info(f" - Telescope was moved to RA = {RA:07.4f} h, " + \
                     f"Dec = {Dec:07.4f} deg.")
        
        # And remove this entry from the list.
        schedule.pop(0)

     time.sleep(1.0)
     current_time = current_time + timedelta(seconds=1)

# ----------------------------------------------------------------------------
# 6. Give a message and quit.
# ----------------------------------------------------------------------------
logging.warning("Input file ran out of data. Quitting.")
print("Input file ran out of data. Quitting.")
