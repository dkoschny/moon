"""
Filename: get_moon_geometry.py

"""
import requests  # To read data from the internet


def get_moon_geometry(start_time, stop_time, d_time, obs_loc):
    """Get the subsolar point and the sub-observer point on the Moon,
       in selenographic coordinates, for a given time span.

    Version
    -------
    V1.0, 27 Dec 2021, dvk - First working version.
    V3.0, 04 Feb 2023, dvk - Add returning RA and Dec - needed to
                             determine relative stream position.
    V3.1, 09 Feb 2023, dvk - and realised that fields 1 and 2 are
                             something else, skip them.

    Open points
    -----------
    2021-12-27T12:11 - 3-tuple for observing location still to implement.
    2021-12-27T15:30 - Error checking of input data is currently not
                       yet performed.

    Parameters
    ----------
    start_time : string
        Start time of requested data, in the form 'yyyy-mm-ddThh:mm:ss'.

    stop_time : string
        End time of requested data, in the form 'yyyy-mm-ddThh:mm:ss'.

    d_time : string
        Delta time, i.e. the time step, in which to generate data.
        Use a number, a space, then 'y' for years, 'm' for months, 'd'
        for days, 'h' for hours, 'm' for minutes. NOTE: If the white
        space doesn't work, use the html encoding %20 instead.
        Example: '2%20h' would be steps of 2 hours.

    obs_loc : string
        The observing location. You have two options:
        (a) The IAU observation code or the SPICE number (e.g. B12 for
            The Koschny Observatory in the Netherlands; ``500`` for the
            Earth center. To find allowable values, go to the JPL Horizons
            web site. G01 Oldenburg
        (b) A 3-tuple of values, denoting longitude, latitude, and height
            above 0.

    Returns
    -------
    time : list of string
        The time of the output data.

    RA, Dec : lists of float
        Right Ascension and Declination of the Moon for the given time,
        in decimal degrees.

    obs_sub_lat, obs_sub_lon : list of float
        Lunar sub-latitude and -longitude of the observer,
        in decimal degrees.

    sun_sub_lat, sun_sub_lon : list of float
        Lunar sub-latitude and -longitude of the Sun, in decimal degrees.
        
    dist : float
        Distance between the center of the Moon and the observer in km.

    Usage
    -----
    time, ra, dec, obs_sun_lat, obs_sun_long, \
    sun_sub_lat, sun_sub_long = get_moon_geometry(start_time, end_time, d_time,
                                              obs_loc)
    Method
    ------
    This routine takes start and end date and a time interval as input. It
    allows entering the observer's location, either as IAU station code, or
    longitude, latitude and elevation. It will query the JPL Horizons serice,
    using the REST interface as described here:

    https://ssd-api.jpl.nasa.gov/doc/horizons.html

    This routine is indended to be used e.g. in the Lunar Impact Flash
    Simulator tool developed by Max Klass, Uni Oldenburg, to allow testing of
    the NELIOTA Flash Detection Software. Of course it might also come in handy
    for other projects.

    The following quantities are returned:
    1:  Astrometric RA & Dec (used as of V3.0 to compute relative stream direction)
    10: Illuminated fraction (currently not used)
    14: Observer sub-lon & sub-lat
    15: Sun sub-longitude & sub-latitude
    20: Observer range & range-rate (to the center of the Moon)

    Import this routine into other code by saying:

    from get_moon_geometry import get_moon_geometry

    Just running this routine will call a test routine that generates
    some output to the console.

    """
    url = "https://ssd.jpl.nasa.gov/api/horizons.api"
    au = 149597900  # Astronomical unit in km

    # Check that start and end time follows the convention.
    # !TODO

    # Check that the d_time follows the requirements - a number, followed by
    # d, h, m, y, mo.
    # !TODO

    # Build the query. Note: The '301' in the COMMAND keyword is the Moon.
    query = url + f"?format=text&COMMAND='301'&OBJ_DATA='YES'" + \
            f"&MAKE_EPHEM='YES'&EPHEM_TYPE='OBSERVER'&CENTER='{obs_loc}'" + \
            f"&START_TIME='{start_time}'&STOP_TIME='{stop_time}'" + \
            f"&STEP_SIZE='{d_time}'&ANG_FORMAT='DEG'&TIME_DIGITS='SECONDS'" + \
            f"&QUANTITIES='1,10,14,15,20'&CSV_FORMAT='YES'"

    print(query)

    response = requests.get(query)
    print(response.status_code)

    if not response.ok:
        print("Problem reading the JPL Horizons page. Malformed query.")
        quit()

    # Extract the data from the response.
    # First: Split into lines.
    lines = []
    for line in response.text.split("\n"):
        lines.append(line)

    # Now move to the line after the string ``$$SOE`` and end at ``$$EOE``
    # and only append those to the array ``final_data``.
    final_data = []
    append = 'no'
    for line in lines:
        if append == "yes":
            final_data.append(line)
        if line == "$$SOE":
            # Key to indicate `start of ephemerides`
            append = "yes"
        if line == "$$EOE":
            # Key to indicate `end of ephemerides`
            append = "no"

    # Split the line into the different elements.
    time, RA, Dec, illu = [], [], [], []
    obs_sub_lat, obs_sub_lon, sun_sub_lat, sun_sub_lon = [], [], [], []
    dist, dist_dot = [], []

    for line in final_data[0: -1]:  # Don't use the last line
        # print(line)
        time.append(line.split(',')[0])
        RA.append(line.split(',')[3])
        Dec.append(line.split(',')[4])
        # illu.append(line.split(',')[5])
        obs_sub_lon.append(float(line.split(',')[6]))
        obs_sub_lat.append(float(line.split(',')[7]))
        sun_sub_lon.append(float(line.split(',')[8]))
        sun_sub_lat.append(float(line.split(',')[9]))
        dist.append(float(line.split(',')[10]) * au)
        dist_dot.append(float(line.split(',')[11]) * au)

    return time, RA, Dec, obs_sub_lat, obs_sub_lon, \
           sun_sub_lat, sun_sub_lon, dist


# ============================= Test routine ==================================
if __name__ == "__main__":
    # If this routine is called directly, the following test is executed.
    start_time = "2022-01-25T21:00"
    stop_time = "2022-01-25T23:00"
    d_time = "1%20h"
    obs_loc = "J04"
    time, RA, Dec, obs_sub_lat, obs_sub_lon, \
    sun_sub_lat, sun_sub_lon, dist, line = get_moon_geometry(start_time, stop_time,
                                                       d_time, obs_loc)
    # print a nicely formatted list
    print()
    print("-----------------------------------------------------------------------------------")
    print("       time             RA     Dec    obs sub-point Sun sub-point    distance")
    print("                                       long   lat    long   lat       in km")
    print("-----------------------------------------------------------------------------------")
    for i in range(len(time)):
        print(f"{time[i]:20} " + \
              f"{float(RA[i]):4.4f} " + \
              f"{float(Dec[i]):3.4f} " + \
              f"{float(obs_sub_lon[i]):06.2f} " + \
              f"{float(obs_sub_lat[i]):06.2f} " + \
              f"{float(sun_sub_lon[i]):06.2f} " + \
              f"{float(sun_sub_lat[i]):06.2f} " + \
              f" {round(float(dist[i]), 2):12.2f} ")
    print("-----------------------------------------------------------------------------------")

# =================================== EOF =====================================
