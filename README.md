# Detlef's "Moon" repository.

This project contains a number of Python routines I have written to help me observing the Moon. In particular this is about enabling the observation of impact flashes on the dark (unilluminated) side of the Moon.

For now, the main routines are:

- get_moon_geometry.py - gets information about the Moon from a given observation site (must have an IAU observatory code);
- get_moon_ephemerides.py - a command-line tool that uses the previous routine to generate a csv file with time/RA/Dec info about the Moon, for a given time span and location (use an IAU observatory closest to you if you don't have your own code);
- track_moon.py - a Python routine which commands an ASCOM telescope mount to go to the Moon position in one-minute intervals.

Stay tuned, dvk (23 Jan 2022)
